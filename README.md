#Documentation 

1. Dependencies
2. Package Structure
3. Testing
4. Technology
5. Domain Map
6. Flow
7. TODO

##1.Dependencies

- Mysql
- Docker; we need docker installed to be able to run Mysql Container, so we can run application and integration test
- Maven (Optional), if you do not have maven installed, there is one downloaded runnable for you `./mvnw`


##2.Package Structure

###2.1.Project URLs
API Documentation(`/documentation/api_doc.png`): You can interact with service from UI from following URL:
  - `http://park-test.herokuapp.com/api/swagger-ui.html`

Database: Google Cloud Platform, MySQL Server 
    - URL: jdbc:mysql://35.200.237.180:3306/park 
    - username: admin
    - password: admin    

###2.2.Run the Application

To Run Application, you only need to have a terminal,
and you need to run 2 command in series:

    ./mvnw clean install 
    
    ./mvnw spring-boot:run
    
Dependencies:

To be able to run the application you need to have Mysql database up and running.

For Local Environment you can use command to have quick and easy Mysql database:
    
    docker run -p 3306:3306 --name test-mysql -e MYSQL_DATABASE=park  -e MYSQL_ROOT_PASSWORD=admin  -e MYSQL_USER=admin -e MYSQL_PASSWORD=admin -d mysql 
    
##3.Test the Application

To be able to test the application you need to have Mysql database up and running.

To Run Application, you only need to have a terminal,
and you need to run following command in series:

    ./mvnw test    

For testing purposes there are 2 types of testing approaches have been used:
- End to End Test: Used for `Happy Path` scenarios
- Unit Test: Used for rules, constraints and edge cases. Mostly used on Business heavily components. 
 
 
##4.Technology

- Java 11
- Spring Boot -> Web Server 
- Swagger -> API Documentation
- Lombok -> Boilerplate Code Library
- Hibernate -> ORM Library for database connections
- Flyway -> Database Migration Library


##5.Domain Map
Domain Map for parking application is like following;
You can find more at `documentation/domain_map.png`

   	- Reservation: Created time slot on spesific parking lot 
   	- Parking: Group of Lots where users can book
   	- ParkingLot: Physical space where users can book for certain period of time
   	
Note: Since project scope is small and system needs to understand identity,
to provide uniqueness reservation made by `phoneNumber` 
 
   	
##6.Flow

###6.1.Constrains & Rules
1- Each Reservation has fixed interval and cost: 3 Hours, 5 USD
2- Parking Slots should be `dynamicaly` assigned to user
3- Reservation can only be made by 15 minutes intervals
4- When searching available slot, system should suggest closest slot in same parking or other parkings.

###6.2.Main Flows

System have 3 Main Flow:
- Search Park Slot Availabilities
- Reservation Creation
- Park Slot Creation

####6.2.1.Flow: Search Park Slot Availabilities
1- Retrieve All Reservations On Selected Time and Parking
2- Create Red-Black Tree for Parking and Availabilities each items are created by `15 Minutes` interval over the day 
3- Fill All The Tree Nodes Where Reservation has not been made.
4- Check If Tree Has Available Nodes Either Selected Slot Or Closer to Selected Node
5- If Lot Is Not Found On Selected Parking, System Will Look Same Procedure For Other Parkings.    

####6.2.2.Flow: Reservation Creation
1- Check Following conditions(ReservationValidationService):
    1.1- Validate Reservation Start Time (15 Minutes Interval)
    1.2- Validate Parking Exist
    1.3- Validate Reservation Not Exist
2- Find Slot From Database
3- If There Is Any Lot, Run Exception
4- If There Is Lot, Create Reservation        

####6.2.3.Flow: Park Slot Creation
1- Check Following conditions(ReservationValidationService):
    1.1- Validate Park Exist
    1.2- Validate Parking Lot Not Exist
2- Create Lot For Selected Parking
   	   
##7.TODO
1- Logging
2- Unit Tests for the Validation and Business Services
3- Creating User Domain
4- Security Gateway