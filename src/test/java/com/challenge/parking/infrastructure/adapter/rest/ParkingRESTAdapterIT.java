package com.challenge.parking.infrastructure.adapter.rest;

import com.challenge.parking.ParkingApplication;
import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.entity.Parking;
import com.challenge.parking.domain.model.entity.ParkingLot;
import com.challenge.parking.domain.model.entity.Reservation;
import com.challenge.parking.domain.repository.ParkingLotRepository;
import com.challenge.parking.domain.repository.ParkingRepository;
import com.challenge.parking.domain.repository.ReservationRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertNotNull;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ParkingApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableTransactionManagement
class ParkingRESTAdapterIT {

    @LocalServerPort
    private int port;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private ParkingLotRepository parkingLotRepository;

    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();


    @BeforeEach
    public void init() {
        Parking parking1 = new Parking();
        Parking parking2 = new Parking();
        ParkingLot parking1LotA = new ParkingLot();
        ParkingLot parking1LotB = new ParkingLot();
        ParkingLot parking2LotA = new ParkingLot();
        ParkingLot parking2LotB = new ParkingLot();

        List<Parking> parkingList = new ArrayList<>();
        List<ParkingLot> parking1LotList = new ArrayList<>();
        List<ParkingLot> parking2LotList = new ArrayList<>();

        parking1LotA.setName("A");
        parking1LotB.setName("B");
        parking2LotA.setName("A");
        parking2LotB.setName("B");

        parking1LotA.setParking(parking1);
        parking1LotB.setParking(parking1);
        parking1LotList.add(parking1LotA);
        parking1LotList.add(parking1LotB);

        parking2LotA.setParking(parking2);
        parking2LotB.setParking(parking2);
        parking2LotList.add(parking2LotA);
        parking2LotList.add(parking2LotB);

        parking1.setName("Marina Bay");
        parking1.setAddress("Marina Bay");
        parking1.setParkingLotList(parking1LotList);

        parking2.setName("Marina Financial");
        parking2.setAddress("Marina Financial");
        parking2.setParkingLotList(parking2LotList);

        parkingList.add(parking1);
        parkingList.add(parking2);

        List<Parking> persistedParkingList = parkingRepository.saveAll(parkingList);
        Parking parking = persistedParkingList.get(0);

        Reservation reservation = new Reservation();
        reservation.setCost(BigDecimal.TEN);
        reservation.setOrderTime(LocalDateTime.of(2020, 06, 06, 11, 00));
        reservation.setStartTime(LocalDateTime.of(2020, 06, 06, 15, 00));
        reservation.setEndingTime(LocalDateTime.of(2020, 06, 06, 18, 00));
        reservation.setPhoneNumber("+6588888888");
        reservation.setParkingLot(parking.getParkingLotList().get(0));
        reservationRepository.save(reservation);
    }

    @AfterEach
    public void tearDown() {
        reservationRepository.deleteAll();
        parkingRepository.deleteAll();
    }

    @Test
    void retrieveParkingList() {
        //given
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        //when
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/parkings"),
                HttpMethod.GET, entity, String.class);

        //then
        assertNotNull("Body Cannot be null", response.getBody());
        assertTrue(response.getBody(), response.getBody().contains("Marina Bay"));
    }

    @Test
    void searchParkingLotAvailabilities() {
        //given
        List<Parking> parkingList = parkingRepository.findAll();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(createURLWithPort("/parkings/availabilities"))
                .queryParam("parkingID", parkingList.get(0).getId())
                .queryParam("date", "2020-06-06")
                .queryParam("startTime", "15:15");

        //when
        ResponseEntity<String> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET, entity, String.class);

        //then
        assertNotNull("Body Cannot be null", response.getBody());
        assertTrue(response.getBody(), response.getBody().contains("success"));
        assertTrue(response.getBody(), response.getBody().contains("Marina Financial"));
        assertTrue(response.getBody(), response.getBody().contains("2020-06-06T15:15:00"));
    }

    @Test
    void createParkingLot() {
        //given
        CreateParkingLotRequestDTO createParkingLotRequestDTO = new CreateParkingLotRequestDTO();
        createParkingLotRequestDTO.setParkingID(1L);
        createParkingLotRequestDTO.setParkingLotName("Mock Lot Name");
        HttpEntity<CreateParkingLotRequestDTO> entity = new HttpEntity<CreateParkingLotRequestDTO>(createParkingLotRequestDTO, headers);

        //when
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/parkings"),
                HttpMethod.POST, entity, String.class);

        //then
        assertNotNull("Body Cannot be null", response.getBody());
        assertTrue(response.getBody(), response.getBody().contains("success"));
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + "/api/v1" + uri;
    }
}