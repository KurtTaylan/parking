package com.challenge.parking.domain.service.valitation;

import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.exception.BusinessException;
import com.challenge.parking.domain.repository.ParkingLotRepository;
import com.challenge.parking.domain.repository.ParkingRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@AllArgsConstructor
@Service
public class ParkingValidationService {

    private final ParkingRepository parkingRepository;
    private final ParkingLotRepository parkingLotRepository;

    public void apply(CreateParkingLotRequestDTO createParkingLotRequestDTO) {
        validateParkExist(createParkingLotRequestDTO);
        validateParkingLotNotExist(createParkingLotRequestDTO);
    }

    private void validateParkExist(CreateParkingLotRequestDTO createParkingLotRequestDTO) {
        boolean isParkingExist = parkingRepository.existsById(createParkingLotRequestDTO.getParkingID());
        if (!isParkingExist) {
            throw new BusinessException(
                    "Parking is not Found! by given parking ID: " +
                            createParkingLotRequestDTO.getParkingID());
        }
    }

    private void validateParkingLotNotExist(CreateParkingLotRequestDTO createParkingLotRequestDTO) {
        boolean isParkingLotExist = parkingLotRepository
                .findByNameAndParkingId(createParkingLotRequestDTO.getParkingLotName(),
                        createParkingLotRequestDTO.getParkingID()).isPresent();
        if (isParkingLotExist) {
            throw new BusinessException(
                    "Parking Lot is exist! by given parking ID:" + createParkingLotRequestDTO.getParkingID() + " and Lot name: " +
                            createParkingLotRequestDTO.getParkingLotName());
        }
    }
}
