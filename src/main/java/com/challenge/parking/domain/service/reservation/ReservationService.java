package com.challenge.parking.domain.service.reservation;

import com.challenge.parking.domain.model.dto.request.reservation.CreateReservationRequestDTO;
import com.challenge.parking.domain.model.entity.Parking;
import com.challenge.parking.domain.model.entity.ParkingLot;
import com.challenge.parking.domain.model.entity.Reservation;
import com.challenge.parking.domain.model.exception.BusinessException;
import com.challenge.parking.domain.repository.ParkingLotRepository;
import com.challenge.parking.domain.repository.ReservationRepository;
import com.challenge.parking.infrastructure.configuration.properties.ReservationSettingsProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class ReservationService {

    private final ParkingLotRepository parkingLotRepository;
    private final ReservationRepository reservationRepository;
    private final ReservationSettingsProperty reservationSettingsProperty;

    @Transactional
    public Reservation create(CreateReservationRequestDTO createReservationRequestDTO) {
        LocalDateTime reservationStartTime = LocalDateTime.of(
                createReservationRequestDTO.getDate(),
                createReservationRequestDTO.getStartTime());

        List<ParkingLot> parkingLotList = parkingLotRepository.findAllByParkingId(createReservationRequestDTO.getParkingID());
        Map<LocalDateTime, List<Reservation>> parkingLotReservationMap = retrieveReservationMapGroupedByStartTime(parkingLotList, reservationStartTime);
        Optional<ParkingLot> optionalParkingLot = findAvailableLot(parkingLotList, parkingLotReservationMap);

        if (optionalParkingLot.isPresent()) {
            ParkingLot parkingLot = optionalParkingLot.get();
            Reservation reservation = createNewReservation(reservationStartTime, parkingLot, createReservationRequestDTO);
            return reservationRepository.save(reservation);
        } else {
            throw new BusinessException("Available Parking Slot is not found!");
        }
    }

    private Optional<ParkingLot> findAvailableLot(List<ParkingLot> parkingLotList, Map<LocalDateTime, List<Reservation>> parkingLotReservationMap) {
        return parkingLotList.stream().filter(parkingLot -> {
            Optional<LocalDateTime> optionalLocalDateTime = parkingLotReservationMap.keySet().stream()
                    .filter(localDateTime -> {
                        List<Reservation> reservationList = parkingLotReservationMap.get(localDateTime);
                        Optional<Reservation> optionalReservation = reservationList.stream().filter(reservation ->
                                Objects.equals(parkingLot.getId(), reservation.getParkingLot().getId())
                        ).findFirst();
                        return optionalReservation.isPresent();
                    }).findFirst();
            return !optionalLocalDateTime.isPresent();
        }).findFirst();
    }

    public Map<LocalDateTime, List<Reservation>> retrieveReservationMapGroupedByStartTime(List<ParkingLot> parkingLotList, LocalDateTime reservationStartTime) {
        List<Reservation> existingReservationList = reservationRepository
                .findAllByParkingLotInAndEndingTimeBetweenOrderByStartTime(
                        parkingLotList,
                        reservationStartTime,
                        reservationStartTime.plus(reservationSettingsProperty.getValidityMinute(), ChronoUnit.MINUTES));

        return existingReservationList
                .stream()
                .sorted()
                .collect(Collectors.groupingBy(Reservation::getStartTime));
    }

    private Reservation createNewReservation(LocalDateTime reservationStartTime, ParkingLot parkingLot, CreateReservationRequestDTO createReservationRequestDTO) {
        Reservation reservation = new Reservation();
        reservation.setPhoneNumber(createReservationRequestDTO.getPhoneNumber());
        reservation.setCost(BigDecimal.valueOf(reservationSettingsProperty.getCost()));
        reservation.setOrderTime(LocalDateTime.now());
        reservation.setStartTime(reservationStartTime);
        reservation.setEndingTime(reservationStartTime.plus(reservationSettingsProperty.getValidityMinute(), ChronoUnit.MINUTES));
        reservation.setParkingLot(parkingLot);
        Parking parking = parkingLot.getParking();
        return reservation;
    }

    public void deleteAll() {
        reservationRepository.deleteAll();
    }

    public List<Reservation> retrieveAll() {
        return reservationRepository.findAll();
    }
}
