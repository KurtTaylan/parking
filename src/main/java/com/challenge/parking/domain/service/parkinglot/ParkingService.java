package com.challenge.parking.domain.service.parkinglot;

import com.challenge.parking.domain.model.dto.core.ParkingLotAvailabilityDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.SearchParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.SearchParkingLotResponseDTO;
import com.challenge.parking.domain.model.entity.Parking;
import com.challenge.parking.domain.model.entity.ParkingLot;
import com.challenge.parking.domain.model.entity.Reservation;
import com.challenge.parking.domain.model.exception.BusinessException;
import com.challenge.parking.domain.repository.ParkingLotRepository;
import com.challenge.parking.domain.repository.ParkingRepository;
import com.challenge.parking.domain.service.reservation.ReservationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class ParkingService {

    private final ParkingRepository parkingRepository;
    private final ParkingLotRepository parkingLotRepository;
    private final ReservationService reservationService;
    private final ParkingAvailabilityService parkingAvailabilityService;

    @Transactional
    public SearchParkingLotResponseDTO searchLot(SearchParkingLotRequestDTO searchParkingLotRequestDTO) {
        SearchParkingLotResponseDTO searchParkingLotResponseDTO = new SearchParkingLotResponseDTO();
        LocalDateTime reservationStartTime = LocalDateTime.of(
                LocalDate.parse(searchParkingLotRequestDTO.getDate()),
                LocalTime.parse(searchParkingLotRequestDTO.getStartTime()));

        List<Parking> parkingList = parkingRepository.findAll();
        Parking selectedParking = parkingList.stream()
                .filter(parking -> parking.getId() == searchParkingLotRequestDTO.getParkingID())
                .findFirst()
                .get();
        Map<LocalDateTime, List<Reservation>> startTimeReservationMap = reservationService.retrieveReservationMapGroupedByStartTime(selectedParking.getParkingLotList(), reservationStartTime);
        parkingAvailabilityService.findAvailableLot(searchParkingLotRequestDTO.getParkingID(), searchParkingLotResponseDTO, reservationStartTime, selectedParking.getParkingLotList(), startTimeReservationMap);

        ParkingLotAvailabilityDTO selectedParkingLotAvailability = searchParkingLotResponseDTO.getSelectedParkingLotAvailability();
        if (selectedParkingLotAvailability == null) {
            List<Parking> otherParkingList = parkingList.stream().filter(parking -> !parking.getId().equals(searchParkingLotRequestDTO.getParkingID())).collect(Collectors.toList());
            ParkingLotAvailabilityDTO firstBestAvailableOption = searchParkingLotResponseDTO.getNextBestAvailableOption();
            otherParkingList.forEach(otherParking -> {
                parkingAvailabilityService.findAvailableLot(searchParkingLotRequestDTO.getParkingID(), searchParkingLotResponseDTO, reservationStartTime, otherParking.getParkingLotList(), startTimeReservationMap);
                ParkingLotAvailabilityDTO nextBestAvailableCandidate = searchParkingLotResponseDTO.getNextBestAvailableOption();
                parkingAvailabilityService.decideNextBestAvailableOption(searchParkingLotResponseDTO, reservationStartTime, firstBestAvailableOption, nextBestAvailableCandidate);
            });
        }
        searchParkingLotResponseDTO.setStatus("success");
        return searchParkingLotResponseDTO;
    }

    @Transactional
    public ParkingLot createLot(CreateParkingLotRequestDTO createParkingLotRequestDTO) {
        Optional<Parking> optionalParking = parkingRepository.findById(createParkingLotRequestDTO.getParkingID());
        if (optionalParking.isPresent()) {
            Parking parking = optionalParking.get();
            ParkingLot parkingLot = new ParkingLot();
            parkingLot.setName(createParkingLotRequestDTO.getParkingLotName());
            parkingLot.setParking(parking);
            return parkingLotRepository.save(parkingLot);
        } else {
            throw new BusinessException(
                    "Parking is not Found! by given parking ID: " +
                            createParkingLotRequestDTO.getParkingID());
        }
    }

    public List<Parking> retrieveAllParking() {
        return parkingRepository.findAll();
    }
}
