package com.challenge.parking.domain.service.valitation;

import com.challenge.parking.domain.model.dto.request.reservation.CreateReservationRequestDTO;
import com.challenge.parking.domain.model.entity.Parking;
import com.challenge.parking.domain.model.entity.Reservation;
import com.challenge.parking.domain.model.exception.BusinessException;
import com.challenge.parking.domain.repository.ParkingRepository;
import com.challenge.parking.domain.repository.ReservationRepository;
import com.challenge.parking.infrastructure.configuration.properties.ReservationSettingsProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
public class ReservationValidationService {

    private final ReservationSettingsProperty reservationSettingsProperty;
    private final ReservationRepository reservationRepository;
    private final ParkingRepository parkingRepository;

    public void apply(CreateReservationRequestDTO createReservationRequestDTO) {
        LocalDateTime reservationStartTime = LocalDateTime.of(
                createReservationRequestDTO.getDate(),
                createReservationRequestDTO.getStartTime());

        validateReservationStartTime(reservationStartTime);
        validateParkingExist(createReservationRequestDTO);
        validateReservationNotExist(createReservationRequestDTO, reservationStartTime);
    }

    private void validateReservationStartTime(LocalDateTime reservationStartTime) {
        int minute = reservationStartTime.getMinute();
        boolean isNotValidReservationStartTime = minute % reservationSettingsProperty.getBookingIntervalMinute() != 0;
        if (isNotValidReservationStartTime) {
            throw new BusinessException("Reservation start time is not valid! It should be quarter based");
        }
    }

    private void validateParkingExist(CreateReservationRequestDTO createReservationRequestDTO) {
        Optional<Parking> optionalParking = parkingRepository.findById(createReservationRequestDTO.getParkingID());
        if (!optionalParking.isPresent()) {
            throw new BusinessException("Parking is not exist by given Id");
        }
    }

    private void validateReservationNotExist(CreateReservationRequestDTO createReservationRequestDTO, LocalDateTime reservationStartTime) {
        Parking parking = parkingRepository.findById(createReservationRequestDTO.getParkingID()).get();
        List<Reservation> reservationList = reservationRepository
                .findByParkingLotInAndPhoneNumberAndEndingTimeGreaterThan(
                        parking.getParkingLotList(),
                        createReservationRequestDTO.getPhoneNumber(),
                        reservationStartTime);

        if (!reservationList.isEmpty()) {
            throw new BusinessException("Reservation is exist for following 3 Hours slot by given; Phone Number, Parking and Start Time!");
        }
    }
}
