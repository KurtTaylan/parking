package com.challenge.parking.domain.service.parkinglot;

import com.challenge.parking.domain.model.dto.core.ParkingLotAvailabilityDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.SearchParkingLotResponseDTO;
import com.challenge.parking.domain.model.entity.ParkingLot;
import com.challenge.parking.domain.model.entity.Reservation;
import com.challenge.parking.infrastructure.configuration.properties.ReservationSettingsProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class ParkingAvailabilityService {

    private final ReservationSettingsProperty reservationSettingsProperty;

    public void findAvailableLot(Long selectedParkID, SearchParkingLotResponseDTO searchParkingLotResponseDTO, LocalDateTime reservationStartTime, List<ParkingLot> parkingLotList, Map<LocalDateTime, List<Reservation>> startTimeReservationMap) {
        TreeMap<LocalTime, List<ParkingLot>> availabilityParkingLot = createAvailabilitySlotTree(parkingLotList);
        TreeMap<LocalTime, List<ParkingLot>> finalAvailabilityParkingLot = fillAvailabilityParkingLotTree(startTimeReservationMap, availabilityParkingLot);
        if (!finalAvailabilityParkingLot.isEmpty()) {
            setAvailableLotFromSelectedParking(selectedParkID, searchParkingLotResponseDTO, reservationStartTime, finalAvailabilityParkingLot);
        }
    }

    public TreeMap<LocalTime, List<ParkingLot>> createAvailabilitySlotTree(List<ParkingLot> parkingLotList) {
        TreeMap<LocalTime, List<ParkingLot>> availabilityParkingLot = new TreeMap<>();
        int slotCount = 24 * 60 / reservationSettingsProperty.getBookingIntervalMinute();
        LocalTime startingTime = LocalTime.of(0, 0);
        for (int i = 0; i <= slotCount; i++) {
            LocalTime nextTimeSlot = startingTime.plus(i * reservationSettingsProperty.getBookingIntervalMinute(), ChronoUnit.MINUTES);
            availabilityParkingLot.put(nextTimeSlot, parkingLotList);
        }
        return availabilityParkingLot;
    }

    public TreeMap<LocalTime, List<ParkingLot>> fillAvailabilityParkingLotTree(Map<LocalDateTime, List<Reservation>> startTimeReservationMap, TreeMap<LocalTime, List<ParkingLot>> availabilityParkingLot) {
        startTimeReservationMap.keySet().forEach(localDateTime -> {
            List<Reservation> reservationList = startTimeReservationMap.get(localDateTime);
            reservationList.forEach(reservation -> {
                ParkingLot reservationParkingLot = reservation.getParkingLot();
                LocalTime startTime = reservation.getStartTime().toLocalTime().minus(reservationSettingsProperty.getValidityMinute(), ChronoUnit.MINUTES);
                int timeSlotCount = (reservationSettingsProperty.getValidityMinute() * 2) / reservationSettingsProperty.getBookingIntervalMinute();
                for (int i = 1; i < timeSlotCount; i++) {
                    LocalTime counterTime = startTime.plus(i * reservationSettingsProperty.getBookingIntervalMinute(), ChronoUnit.MINUTES);
                    List<ParkingLot> parkingLotCandidateList = availabilityParkingLot.get(counterTime);
                    List<ParkingLot> newList = parkingLotCandidateList.stream()
                            .filter(parkingLot -> !Objects.equals(parkingLot.getId(), reservationParkingLot.getId()))
                            .collect(Collectors.toList());
                    availabilityParkingLot.put(counterTime, newList);
                }
            });
        });

        TreeMap<LocalTime, List<ParkingLot>> finalAvailabilityParkingLot = new TreeMap<>();
        availabilityParkingLot.keySet().stream()
                .filter(localTime -> {
                    List<ParkingLot> availabilityParkingLotList = availabilityParkingLot.get(localTime);
                    return !availabilityParkingLotList.isEmpty();
                }).sorted()
                .forEach(localTime -> {
                    List<ParkingLot> availabilityParkingLotList = availabilityParkingLot.get(localTime);
                    finalAvailabilityParkingLot.put(localTime, availabilityParkingLotList);
                });

        return finalAvailabilityParkingLot;
    }

    public void setAvailableLotFromSelectedParking(Long selectedParkID, SearchParkingLotResponseDTO searchParkingLotResponseDTO, LocalDateTime reservationStartTime, TreeMap<LocalTime, List<ParkingLot>> finalAvailabilityParkingLot) {
        LocalTime reservationLocalTime = reservationStartTime.toLocalTime();
        List<ParkingLot> parkingLotList = finalAvailabilityParkingLot.values().stream().filter(parkingLots -> !parkingLots.isEmpty()).findAny().get();
        ParkingLot parkingLot = parkingLotList.get(0);

        if (selectedParkID == parkingLot.getParking().getId() && finalAvailabilityParkingLot.containsKey(reservationLocalTime)) {
            List<ParkingLot> parkingLotList1 = finalAvailabilityParkingLot.get(reservationLocalTime);
            ParkingLotAvailabilityDTO newParkingLotAvailability = createNewParkingLotAvailability(parkingLotList1.get(0), reservationStartTime);
            searchParkingLotResponseDTO.setSelectedParkingLotAvailability(newParkingLotAvailability);
        } else {
            LocalTime floorTime = finalAvailabilityParkingLot.floorKey(reservationLocalTime);
            LocalTime ceilingTime = finalAvailabilityParkingLot.ceilingKey(reservationLocalTime);

            int reservationLocalTimeSecond = reservationLocalTime.toSecondOfDay();
            int floorKeySecond = floorTime.toSecondOfDay();
            int ceilingKeySecond = ceilingTime.toSecondOfDay();

            int floorDifference = reservationLocalTimeSecond - floorKeySecond;
            int ceilingDifference = ceilingKeySecond - reservationLocalTimeSecond;

            if (ceilingDifference <= floorDifference) {
                List<ParkingLot> closestLaterParkSlotList = finalAvailabilityParkingLot.get(ceilingTime);
                ParkingLotAvailabilityDTO newParkingLotAvailability = createNewParkingLotAvailability(
                        closestLaterParkSlotList.get(0),
                        LocalDateTime.of(reservationStartTime.toLocalDate(), ceilingTime));
                searchParkingLotResponseDTO.setNextBestAvailableOption(newParkingLotAvailability);
            } else {
                List<ParkingLot> closestEarlyParkSlotList = finalAvailabilityParkingLot.get(floorTime);
                ParkingLotAvailabilityDTO newParkingLotAvailability = createNewParkingLotAvailability(
                        closestEarlyParkSlotList.get(0),
                        LocalDateTime.of(reservationStartTime.toLocalDate(), floorTime));
                searchParkingLotResponseDTO.setNextBestAvailableOption(newParkingLotAvailability);
            }
        }
    }

    public void decideNextBestAvailableOption(SearchParkingLotResponseDTO searchParkingLotResponseDTO, LocalDateTime reservationStartTime, ParkingLotAvailabilityDTO firstBestAvailableOption, ParkingLotAvailabilityDTO nextBestAvailableCandidate) {
        int reservationStartTimeSeconds = reservationStartTime.toLocalTime().toSecondOfDay();
        int firstBestAvailableOptionSeconds = firstBestAvailableOption.getSlotAvailabilityStartTime().toLocalTime().toSecondOfDay();
        int nextBestAvailableCandidateSeconds = nextBestAvailableCandidate.getSlotAvailabilityStartTime().toLocalTime().toSecondOfDay();

        int firstBestAvailableOptionDiff = Math.abs(reservationStartTimeSeconds - firstBestAvailableOptionSeconds);
        int nextBestAvailableOptionDiff = Math.abs(reservationStartTimeSeconds - nextBestAvailableCandidateSeconds);

        if (firstBestAvailableOptionDiff <= nextBestAvailableOptionDiff) {
            searchParkingLotResponseDTO.setNextBestAvailableOption(firstBestAvailableOption);
        }
    }

    public ParkingLotAvailabilityDTO createNewParkingLotAvailability(ParkingLot parkingLot, LocalDateTime reservationStartTime) {
        ParkingLotAvailabilityDTO selectedParkingLotAvailability = new ParkingLotAvailabilityDTO();
        selectedParkingLotAvailability.setSlotAvailabilityStartTime(reservationStartTime);
        selectedParkingLotAvailability.setParkingID(parkingLot.getParking().getId());
        selectedParkingLotAvailability.setParkingName(parkingLot.getParking().getName());
        selectedParkingLotAvailability.setParkingLotID(parkingLot.getId());
        selectedParkingLotAvailability.setParkingLotName(parkingLot.getName());
        return selectedParkingLotAvailability;
    }
}
