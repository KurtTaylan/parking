package com.challenge.parking.domain.repository;

import com.challenge.parking.domain.model.entity.ParkingLot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long> {

    Optional<ParkingLot> findByNameAndParkingId(String name, Long parkingId);

    List<ParkingLot> findAllByParkingId(Long parkingId);
}
