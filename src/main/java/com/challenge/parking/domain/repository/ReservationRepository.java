package com.challenge.parking.domain.repository;

import com.challenge.parking.domain.model.entity.ParkingLot;
import com.challenge.parking.domain.model.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findAllByParkingLotInAndEndingTimeBetweenOrderByStartTime(List<ParkingLot> parkingLotList, LocalDateTime endTime, LocalDateTime expectedEndingTime);

    List<Reservation> findByParkingLotInAndPhoneNumberAndEndingTimeGreaterThan(
            List<ParkingLot> parkingLotList,
            String phoneNumber,
            LocalDateTime endingTime);
}
