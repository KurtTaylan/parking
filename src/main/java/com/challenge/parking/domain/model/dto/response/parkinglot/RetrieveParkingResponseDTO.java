package com.challenge.parking.domain.model.dto.response.parkinglot;

import com.challenge.parking.domain.model.dto.core.ParkingDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RetrieveParkingResponseDTO {

    private String status;
    private List<ParkingDTO> parkingList = new ArrayList<>();
}
