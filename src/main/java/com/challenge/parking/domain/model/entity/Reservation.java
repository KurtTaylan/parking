package com.challenge.parking.domain.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@EqualsAndHashCode(of = {"phoneNumber", "parking", "parkingLot"})
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "reservation")
public class Reservation implements Comparable<Reservation> {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "order_date_time")
    private LocalDateTime orderTime;

    @Column(name = "start_time")
    private LocalDateTime startTime;

    @Column(name = "ending_time")
    private LocalDateTime endingTime;

    @Column(name = "cost")
    private BigDecimal cost;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parking_lot_id")
    private ParkingLot parkingLot;

    @Override
    public int compareTo(Reservation reservation) {
        return reservation.getStartTime()
                .compareTo(this.getStartTime());
    }
}
