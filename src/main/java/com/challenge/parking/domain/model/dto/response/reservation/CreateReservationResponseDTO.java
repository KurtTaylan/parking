package com.challenge.parking.domain.model.dto.response.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class CreateReservationResponseDTO {

    private String status;
    private Long reservationID;
    private LocalDateTime reservationStartTime;
    private String parkName;
    private String parkLotName;
}
