package com.challenge.parking.domain.model.dto.core;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ParkingLotAvailabilityDTO {

    private Long parkingID;
    private String parkingName;
    private Long parkingLotID;
    private String parkingLotName;
    private LocalDateTime slotAvailabilityStartTime;
}
