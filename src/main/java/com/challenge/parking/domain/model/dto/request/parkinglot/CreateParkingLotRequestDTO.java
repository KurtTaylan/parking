package com.challenge.parking.domain.model.dto.request.parkinglot;

import lombok.Data;

@Data
public class CreateParkingLotRequestDTO {

    private Long parkingID;
    private String parkingLotName;
}
