package com.challenge.parking.domain.model.dto.response.reservation;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DeleteAllReservationResponseDTO {

    private String status;
}
