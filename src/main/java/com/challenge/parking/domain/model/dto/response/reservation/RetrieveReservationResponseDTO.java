package com.challenge.parking.domain.model.dto.response.reservation;

import com.challenge.parking.domain.model.dto.core.ReservationDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RetrieveReservationResponseDTO {

    private String status;
    private List<ReservationDTO> reservationList = new ArrayList<>();
}
