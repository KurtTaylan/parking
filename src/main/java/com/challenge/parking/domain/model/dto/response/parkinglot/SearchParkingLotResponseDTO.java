package com.challenge.parking.domain.model.dto.response.parkinglot;

import com.challenge.parking.domain.model.dto.core.ParkingLotAvailabilityDTO;
import lombok.Data;

@Data
public class SearchParkingLotResponseDTO {

    private String status;
    private ParkingLotAvailabilityDTO selectedParkingLotAvailability;
    private ParkingLotAvailabilityDTO nextBestAvailableOption;
}
