package com.challenge.parking.domain.model.dto.request.parkinglot;

import lombok.Data;

@Data
public class SearchParkingLotRequestDTO {

    private Long parkingID;
    private String date;
    private String startTime;
}
