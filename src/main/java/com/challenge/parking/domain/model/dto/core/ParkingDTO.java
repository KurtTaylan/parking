package com.challenge.parking.domain.model.dto.core;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ParkingDTO {

    private Long parkingId;
    private String parkingName;
    private List<ParkingLotDTO> parkingLotList;
}
