package com.challenge.parking.domain.model.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@EqualsAndHashCode(of = "name")
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "parking_lot")
public class ParkingLot implements Comparable<ParkingLot> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "parking_id")
    private Parking parking;

    @Override
    public int compareTo(ParkingLot parkingLot) {
        return parkingLot.getName().compareTo(this.name);
    }
}
