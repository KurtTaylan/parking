package com.challenge.parking.domain.model.dto.response.parkinglot;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CreateParkingLotResponseDTO {

    private String status;
    private Long parkingLotID;
}
