package com.challenge.parking.domain.model.dto.request.reservation;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class CreateReservationRequestDTO {

    private Long parkingID;
    private String phoneNumber;
    private LocalDate date;
    private LocalTime startTime;
}
