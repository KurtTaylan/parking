package com.challenge.parking.domain.model.dto.core;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ParkingLotDTO {

    private Long parkingLotId;
    private String parkingLotName;
}
