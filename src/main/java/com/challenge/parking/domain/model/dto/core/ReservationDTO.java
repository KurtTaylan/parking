package com.challenge.parking.domain.model.dto.core;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class ReservationDTO {

    private Long id;
    private String phoneNumber;
    private LocalDateTime orderTime;
    private LocalDateTime startTime;
    private LocalDateTime endingTime;
    private BigDecimal cost;
    private Long parkingId;
    private String parkingName;
    private Long parkingLotId;
    private String parkingLotName;
}
