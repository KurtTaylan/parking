package com.challenge.parking.application.port;

import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.SearchParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.CreateParkingLotResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.RetrieveParkingResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.SearchParkingLotResponseDTO;

public interface ParkingFacadePort {

    SearchParkingLotResponseDTO searchParkingLotAvailabilities(SearchParkingLotRequestDTO searchParkingLotRequestDTO);

    CreateParkingLotResponseDTO createParkingLot(CreateParkingLotRequestDTO createParkingLot);

    RetrieveParkingResponseDTO retrieveParkingList();
}
