package com.challenge.parking.application.port;

import com.challenge.parking.domain.model.dto.request.reservation.CreateReservationRequestDTO;
import com.challenge.parking.domain.model.dto.response.reservation.CreateReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.DeleteAllReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.RetrieveReservationResponseDTO;

public interface ReservationFacadePort {

    CreateReservationResponseDTO createReservation(CreateReservationRequestDTO createReservationRequestDTO);

    RetrieveReservationResponseDTO retrieveReservationList();

    DeleteAllReservationResponseDTO cleanAllReservations();
}
