package com.challenge.parking.application.adapter.facade;

import com.challenge.parking.application.port.ReservationFacadePort;
import com.challenge.parking.domain.model.dto.core.ReservationDTO;
import com.challenge.parking.domain.model.dto.request.reservation.CreateReservationRequestDTO;
import com.challenge.parking.domain.model.dto.response.reservation.CreateReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.DeleteAllReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.RetrieveReservationResponseDTO;
import com.challenge.parking.domain.model.entity.Reservation;
import com.challenge.parking.domain.service.reservation.ReservationService;
import com.challenge.parking.domain.service.valitation.ReservationValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class ReservationFacadeAdapter implements ReservationFacadePort {

    private final ReservationValidationService reservationValidationService;
    private final ReservationService reservationService;


    @Override
    public CreateReservationResponseDTO createReservation(CreateReservationRequestDTO createReservationRequestDTO) {
        reservationValidationService.apply(createReservationRequestDTO);
        Reservation reservation = reservationService.create(createReservationRequestDTO);

        return new CreateReservationResponseDTO(
                "success",
                reservation.getId(),
                reservation.getStartTime(),
                reservation.getParkingLot().getParking().getName(),
                reservation.getParkingLot().getName());
    }

    @Override
    public RetrieveReservationResponseDTO retrieveReservationList() {
        List<Reservation> reservationList = reservationService.retrieveAll();
        List<ReservationDTO> reservationDTOList = mapToDto(reservationList);

        return new RetrieveReservationResponseDTO("success", reservationDTOList);
    }

    @Override
    public DeleteAllReservationResponseDTO cleanAllReservations() {
        reservationService.deleteAll();

        return new DeleteAllReservationResponseDTO("success");
    }

    private List<ReservationDTO> mapToDto(List<Reservation> reservationList) {
        return reservationList.stream().map(reservation -> {
            ReservationDTO reservationDTO = new ReservationDTO();
            reservationDTO.setId(reservation.getId());
            reservationDTO.setPhoneNumber(reservation.getPhoneNumber());
            reservationDTO.setOrderTime(reservation.getOrderTime());
            reservationDTO.setStartTime(reservation.getStartTime());
            reservationDTO.setEndingTime(reservation.getEndingTime());
            reservationDTO.setCost(reservation.getCost());
            reservationDTO.setParkingId(reservation.getParkingLot().getParking().getId());
            reservationDTO.setParkingName(reservation.getParkingLot().getParking().getName());
            reservationDTO.setParkingLotId(reservation.getParkingLot().getId());
            reservationDTO.setParkingLotName(reservation.getParkingLot().getName());
            return reservationDTO;
        }).collect(Collectors.toList());
    }
}
