package com.challenge.parking.application.adapter.facade;

import com.challenge.parking.application.port.ParkingFacadePort;
import com.challenge.parking.domain.model.dto.core.ParkingDTO;
import com.challenge.parking.domain.model.dto.core.ParkingLotDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.SearchParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.CreateParkingLotResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.RetrieveParkingResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.SearchParkingLotResponseDTO;
import com.challenge.parking.domain.model.entity.Parking;
import com.challenge.parking.domain.model.entity.ParkingLot;
import com.challenge.parking.domain.service.parkinglot.ParkingService;
import com.challenge.parking.domain.service.valitation.ParkingValidationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class ParkingFacadeAdapter implements ParkingFacadePort {

    private final ParkingValidationService parkingValidationService;
    private final ParkingService parkingService;


    @Override
    public RetrieveParkingResponseDTO retrieveParkingList() {
        List<Parking> parkingList = parkingService.retrieveAllParking();
        List<ParkingDTO> parkingDTOList = mapToDto(parkingList);
        return new RetrieveParkingResponseDTO("success", parkingDTOList);
    }

    @Override
    public SearchParkingLotResponseDTO searchParkingLotAvailabilities(SearchParkingLotRequestDTO searchParkingLotRequestDTO) {
        return parkingService.searchLot(searchParkingLotRequestDTO);
    }

    @Override
    public CreateParkingLotResponseDTO createParkingLot(CreateParkingLotRequestDTO createParkingLotRequestDTO) {
        parkingValidationService.apply(createParkingLotRequestDTO);
        ParkingLot parkingLot = parkingService.createLot(createParkingLotRequestDTO);

        return new CreateParkingLotResponseDTO("success", parkingLot.getId());
    }

    private List<ParkingDTO> mapToDto(List<Parking> parkingList) {
        return parkingList.stream().map(parking -> {
            List<ParkingLotDTO> parkingLotDTOList = parking.getParkingLotList().stream()
                    .map(parkingLot -> new ParkingLotDTO(parkingLot.getId(), parkingLot.getName()))
                    .collect(Collectors.toList());
            return new ParkingDTO(parking.getId(), parking.getName(), parkingLotDTOList);
        }).collect(Collectors.toList());
    }
}
