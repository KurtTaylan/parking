package com.challenge.parking.infrastructure.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "reservation")
@Data
public class ReservationSettingsProperty {

    private Integer cost;
    private Integer validityMinute;
    private Integer bookingIntervalMinute;
}
