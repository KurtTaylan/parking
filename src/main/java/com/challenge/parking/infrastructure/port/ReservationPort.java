package com.challenge.parking.infrastructure.port;

import com.challenge.parking.domain.model.dto.request.reservation.CreateReservationRequestDTO;
import com.challenge.parking.domain.model.dto.response.reservation.CreateReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.DeleteAllReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.RetrieveReservationResponseDTO;

public interface ReservationPort {

    CreateReservationResponseDTO createReservation(CreateReservationRequestDTO createReservationRequestDTO);

    RetrieveReservationResponseDTO retrieveReservationList();

    DeleteAllReservationResponseDTO cleanAllReservations();
}
