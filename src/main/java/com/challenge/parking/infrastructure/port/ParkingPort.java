package com.challenge.parking.infrastructure.port;

import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.SearchParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.CreateParkingLotResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.RetrieveParkingResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.SearchParkingLotResponseDTO;

public interface ParkingPort {

    RetrieveParkingResponseDTO retrieveParkingList();

    SearchParkingLotResponseDTO searchParkingLotAvailabilities(SearchParkingLotRequestDTO searchParkingLotRequestDTO);

    CreateParkingLotResponseDTO createParkingLot(CreateParkingLotRequestDTO createParkingLotRequestDTO);
}
