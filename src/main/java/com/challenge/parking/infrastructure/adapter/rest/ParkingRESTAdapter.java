package com.challenge.parking.infrastructure.adapter.rest;

import com.challenge.parking.application.port.ParkingFacadePort;
import com.challenge.parking.domain.model.dto.request.parkinglot.CreateParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.request.parkinglot.SearchParkingLotRequestDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.CreateParkingLotResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.RetrieveParkingResponseDTO;
import com.challenge.parking.domain.model.dto.response.parkinglot.SearchParkingLotResponseDTO;
import com.challenge.parking.infrastructure.port.ParkingPort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Api(tags = {"Parking"}, description="Get All Parking List, Creating Parking Lot and Searching Parking Lot Availabilities")
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/v1/parkings")
public class ParkingRESTAdapter implements ParkingPort {

    private final ParkingFacadePort parkingFacadePort;

    @ApiOperation(value = "Retrieve All Parking List With Lots From Database, ", response = RetrieveParkingResponseDTO.class)
    @GetMapping
    @ResponseBody
    @Override
    public RetrieveParkingResponseDTO retrieveParkingList() {
        return parkingFacadePort.retrieveParkingList();
    }

    @ApiOperation(value = "Looking For Best Possible Parking Lot For Given Time", response = SearchParkingLotResponseDTO.class)
    @GetMapping(value = "/availabilities")
    @ResponseBody
    @Override
    public SearchParkingLotResponseDTO searchParkingLotAvailabilities(SearchParkingLotRequestDTO searchParkingLotRequestDTO) {
        return parkingFacadePort.searchParkingLotAvailabilities(searchParkingLotRequestDTO);
    }

    @ApiOperation(value = "Parking Lot Is Creating For Given Parking ID", response = SearchParkingLotResponseDTO.class)
    @PostMapping
    @ResponseBody
    @Override
    public CreateParkingLotResponseDTO createParkingLot(@RequestBody CreateParkingLotRequestDTO createParkingLot) {
        return parkingFacadePort.createParkingLot(createParkingLot);
    }
}
