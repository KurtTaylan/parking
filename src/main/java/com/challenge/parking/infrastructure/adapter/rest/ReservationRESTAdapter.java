package com.challenge.parking.infrastructure.adapter.rest;

import com.challenge.parking.application.port.ReservationFacadePort;
import com.challenge.parking.domain.model.dto.request.reservation.CreateReservationRequestDTO;
import com.challenge.parking.domain.model.dto.response.reservation.CreateReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.DeleteAllReservationResponseDTO;
import com.challenge.parking.domain.model.dto.response.reservation.RetrieveReservationResponseDTO;
import com.challenge.parking.infrastructure.port.ReservationPort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Api(tags = {"Reservation"}, description="Get All Reservation List, Creating Reservation and Deleting Reservation")
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(value = "/v1/reservations")
public class ReservationRESTAdapter implements ReservationPort {

    private final ReservationFacadePort reservationFacadePort;

    @ApiOperation(value = "Create Reservation On Available Time Slot", response = CreateReservationResponseDTO.class)
    @PostMapping
    @ResponseBody
    @Override
    public CreateReservationResponseDTO createReservation(@RequestBody CreateReservationRequestDTO createReservationRequestDTO) {
        return reservationFacadePort.createReservation(createReservationRequestDTO);
    }

    @ApiOperation(value = "Verification End-point: Retrieve All Reservations From Database, ", response = RetrieveReservationResponseDTO.class)
    @GetMapping
    @ResponseBody
    @Override
    public RetrieveReservationResponseDTO retrieveReservationList() {
        return reservationFacadePort.retrieveReservationList();
    }

    @ApiOperation(value = "Delete All Reservations From Database", response = DeleteAllReservationResponseDTO.class)
    @DeleteMapping
    @ResponseBody
    @Override
    public DeleteAllReservationResponseDTO cleanAllReservations() {
        return reservationFacadePort.cleanAllReservations();
    }
}
