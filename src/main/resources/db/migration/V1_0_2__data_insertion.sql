USE park;

INSERT INTO parking (id, name, address)
VALUES
(1, 'Marina Bay', 'Marina Bay'),
(2, 'Marina Financial', 'Marina Financial'),
(3, 'Marina Barrage', 'Marina Barrage');

INSERT INTO parking_lot (id, name, parking_id)
VALUES
(1, 'A', 1),
(2, 'B', 1),
(3, 'A', 2),
(4, 'B', 2),
(5, 'A', 3),
(6, 'B', 3);