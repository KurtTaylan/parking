USE park;

create table park.parking
(
    id bigint auto_increment primary key,
    address varchar(255) null,
    name    varchar(255) null
) engine = InnoDB;

create table park.parking_lot
(
    id bigint auto_increment primary key,
    name       varchar(255) null,
    parking_id bigint       null
) engine = InnoDB;

create index FK_parking_lot_parking
    on park.parking_lot (parking_id);

create table park.reservation
(
    id bigint auto_increment primary key,
    cost            decimal(19, 2) null,
    ending_time     datetime       null,
    order_date_time datetime       null,
    phone_number    varchar(255)   null,
    start_time      datetime       null,
    parking_id      bigint         null,
    parking_lot_id  bigint         null
) engine = InnoDB;

create index FK_reservation_parking
    on park.reservation (parking_id);

create index FK_reservation_parking_lot
    on park.reservation (parking_lot_id);